# Domoticz

Module name: domoticz


## Description

Plugin to communicate with the [Domoticz Home Automation System](http://www.domoticz.com/)


## Config

    "domoticz": {
        host: <string>,       // URL to Domoticz server
        httpport: <number>,   // port number for Domoticz
        log: <boolean>        // show detailed logs
    }


### Config example

    "domoticz": {
        host: "192.168.1.100",
        httpport: 8080,
        log: false
    }

## Author

    Jonas Ingermaa
